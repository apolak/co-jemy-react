var gulp = require('gulp');
var yarn = require('gulp-yarn');
var exec = require('child_process').exec;

gulp.task('build:api', function() {
	return gulp.src(['./api/package.json', './api/yarn.lock']).pipe(yarn())
});

gulp.task('build:front', function() {
	return gulp.src(['./front/package.json', './front/yarn.lock']).pipe(yarn())
});

gulp.task('build:app', ['build:api', 'build:front']);

gulp.task('app:run-dev', function(cb) {
	exec('cd api && yarn run dev-start', function(apiError, apiStdout, apiStdError) {
		console.log("loool");
	});
	exec('cd front && yarn run dev-start', function(err, stdout, stderr) {
		console.log(err);
		console.log(stdout);
		cb(stderr);
	});
});
