import AppComponent from '../app.component';
import { ConnectedSupplierSelectPage } from '../page/supplier-select/connected-supplier-select.page';
import { ConnectedSupplierDetailsPage } from '../page/supplier-details/connected-supplier-details.page';
import { ConnectedOrderDetailsPage } from '../page/order-details/connected-order-details.page';
import { ConnectedGrantAdminPage } from '../page/grant-admin/connected-grant-admin.page';

export const routes = {
	path: '/',
	component: AppComponent,
	indexRoute: { component: ConnectedSupplierSelectPage },
	childRoutes: [
		{
			path: 'supplier/:id',
			component: ConnectedSupplierDetailsPage
		},
		{
			path: 'orders/:id',
			component: ConnectedOrderDetailsPage
		},
		{
			path: 'orders/:id/grant-admin/:hash',
			component: ConnectedGrantAdminPage
		}
	]
};
