import { pl } from './translations/pl';
import { en } from './translations/en';

export const translations = {
	pl: pl,
	en: en
};
