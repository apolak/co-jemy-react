import 'isomorphic-fetch';
import config from 'Config';

const createHeaders = (user, storage) => {
	const headers = new Headers();

	headers.append('Accept', 'application/json');
	headers.append('Content-Type', 'application/json');

	if (user || storage.getItem('user-id')) {
		if (storage.getItem('user-id')) {
			headers.append('user-id', storage.getItem('user-id'));
		} else {
			headers.append('user-id', user);
		}
	}

	return headers;
};

export class OrderClient {
	constructor(config, storage) {
		this.config = config;
		this.storage = storage;
	}

	grantAdmin(user, orderId, adminHash) {
		return fetch(
			config.api.host + '/orders/' + orderId + '/grant/' + adminHash,
			{ headers: createHeaders(user, this.storage), mode: 'cors', method: 'POST' }
		)
			.then(this.handleErrors)
			.then((response) => response.json());
	}

	createOrder(user, supplierId) {
		const body = JSON.stringify({
			supplierId: supplierId
		});

		return fetch(
			config.api.host + '/orders',
			{ headers: createHeaders(user, this.storage), mode: 'cors', method: 'POST', body: body }
		)
			.then(this.handleErrors)
			.then((response) => response.json());
	}

	fetchOrder(user, orderId) {
		return fetch(
			config.api.host + '/orders/' + orderId,
			{ headers: createHeaders(user, this.storage), mode: 'cors', method: 'GET' }
		)
			.then(this.handleErrors)
			.then((response) => response.json());
	}

	addPosition(user, orderId, position) {
		const body = JSON.stringify({
			positionData: position
		});

		return fetch(config.api.host + '/orders/' + orderId + '/positions', {
			headers: createHeaders(user, this.storage),
			mode: 'cors',
			method: 'POST',
			body: body
		})
			.then(this.handleErrors)
			.then((response) => response.json());
	}

	removePosition(user, orderId, positionId) {
		return fetch(
			config.api.host + '/orders/' + orderId + '/positions/' + positionId, {
				headers: createHeaders(user, this.storage),
				mode: 'cors',
				method: 'DELETE'
			}
		)
			.then(this.handleErrors)
			.then((response) => response.json());
	}

	updatePosition(user, orderId, position) {
		const body = JSON.stringify({
			position: position
		});

		return fetch(
			config.api.host + '/orders/' + orderId + '/positions/' + position.id, {
				headers: createHeaders(user, this.storage),
				mode: 'cors',
				method: 'PUT',
				body: body
			}
		)
			.then(this.handleErrors)
			.then((response) => response.json());
	}

	handleErrors(response) {
		if (!response.ok) {
			throw Error(response.statusText);
		}
		return response;
	}

}

export const orderClient = new OrderClient(config, localStorage);
