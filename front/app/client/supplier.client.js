import 'isomorphic-fetch';
import config from 'Config';

class SupplierClient {
	constructor(config) {
		this.config = config;
	}

	fetchSuppliers() {
		return fetch(this.config.api.host + '/suppliers', { mode: 'cors', method: 'GET' })
			.then(this.handleErrors)
			.then((response) => response.json());
	}

	handleErrors(response) {
		if (!response.ok) {
			throw Error(response.statusText);
		}
		return response;
	}

}

export const supplierClient = new SupplierClient(config);
