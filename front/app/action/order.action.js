import { orderClient } from '../client/order.client';
import { push } from 'react-router-redux';

export const CREATE_ORDER_SUCCESS = 'CREATE_ORDER_SUCCESS';
export const CREATE_ORDER_ERROR = 'CREATE_ORDER_ERROR';

export const FETCH_ORDER_SUCCESS = 'FETCH_ORDER_SUCCESS';
export const FETCH_ORDER_ERROR = 'FETCH_ORDER_ERROR';

export const ADD_POSITION_SUCCESS = 'ADD_POSITION_SUCCESS';
export const ADD_POSITION_ERROR = 'ADD_POSITION_ERROR';

export const REMOVE_POSITION_SUCCESS = 'REMOVE_POSITION_SUCCESS';
export const REMOVE_POSITION_ERROR = 'REMOVE_POSITION_ERROR';

export const UPDATE_POSITION_SUCCESS = 'UPDATE_POSITION_SUCCESS';
export const UPDATE_POSITION_ERROR = 'UPDATE_POSITION_ERROR';

export const GRANT_ORDER_ADMIN_SUCCESS = 'GRANT_ADMIN_ORDER_SUCCESS';
export const GRANT_ORDER_ADMIN_ERROR = 'GRANT_ADMIN_ORDER_ERROR';

export const createGrantOrderAdminSuccessAction = (orderId, user) => {
	return {
		type: GRANT_ORDER_ADMIN_SUCCESS,
		orderId: orderId,
		user: user
	};
};

export const createGrantOrderAdminErrorAction = (error) => {
	console.log(error);

	return {
		type: GRANT_ORDER_ADMIN_ERROR,
		error: error
	};
};

export const createGrantOrderAdminAction = (orderId, adminHash) => {
	return (dispatch, getState) => {
		orderClient
			.grantAdmin(getState().user, orderId, adminHash)
			.then((data) => {
				dispatch(createGrantOrderAdminSuccessAction(data.orderId, {
					userId: data.userId,
					adminHash: data.adminHash,
					isAdmin: data.isAdmin
				}));
				dispatch(push('/orders/' + data.orderId));
			})
			.catch((error) => dispatch(createGrantOrderAdminErrorAction(error)));
	};
};

export const createOrderSuccessAction = (orderResponse) => {
	return {
		type: CREATE_ORDER_SUCCESS,
		orderResponse: orderResponse
	};
};

export const createOrderErrorAction = (error) => {
	console.log(error);
	return {
		type: CREATE_ORDER_ERROR,
		error: error
	};
};

export const createOrderAction = (supplierId) => {
	return (dispatch, getState) => {
		orderClient
			.createOrder(getState().user, supplierId)
			.then((data) => {
				dispatch(createOrderSuccessAction(data));
				dispatch(push('/orders/' + data.id));
			})
			.catch((error) => dispatch(createOrderErrorAction(error)));
	};
};

export const createFetchOrderSuccessAction = (orderResponse) => {
	return {
		type: FETCH_ORDER_SUCCESS,
		orderResponse: orderResponse
	};
};

export const createFetchOrderErrorAction = (error) => {
	console.log(error);
	return {
		type: FETCH_ORDER_ERROR,
		error: error
	};
};

export const createFetchOrderAction = (orderId) => {
	return (dispatch, getState) => {
		orderClient
			.fetchOrder(getState().user, orderId)
			.then((data) => dispatch(createFetchOrderSuccessAction(data)))
			.catch((error) => dispatch(createFetchOrderErrorAction(error)));
	};
};

export const createAddPositionSuccessAction = (payload) => {
	return {
		type: ADD_POSITION_SUCCESS,
		payload: payload
	};
};

export const createAddPositionErrorAction = (error) => {
	console.log(error);
	return {
		type: ADD_POSITION_ERROR,
		error: error
	};
};

export const createRemovePositionSuccessAction = (payload) => {
	return {
		type: REMOVE_POSITION_SUCCESS,
		payload: payload
	};
};

export const createRemovePositionErrorAction = (error) => {
	console.log(error);
	return {
		type: REMOVE_POSITION_ERROR,
		error: error
	};
};

export const createAddPositionAction = (orderId, positionData) => {
	return (dispatch, getState) => {
		orderClient
			.addPosition(getState().user, orderId, positionData)
			.then((data) => dispatch(createAddPositionSuccessAction(data)))
			.catch((error) => dispatch(createAddPositionErrorAction(error)));
	};
};

export const createUpdatePositionSuccessAction = (payload) => {
	return {
		type: UPDATE_POSITION_SUCCESS,
		payload: payload
	};
};

export const createUpdatePositionErrorAction = (error) => {
	console.log(error);
	return {
		type: UPDATE_POSITION_ERROR,
		error: error
	};
};

export const createRemovePositionAction = (orderId, positionId) => {
	return (dispatch, getState) => {
		orderClient
			.removePosition(getState().user, orderId, positionId)
			.then((data) => dispatch(createRemovePositionSuccessAction(data)))
			.catch((error) => dispatch(createRemovePositionErrorAction(error)));
	};
};

export const createUpdatePositionAction = (orderId, position) => {
	return (dispatch, getState) => {
		orderClient
			.updatePosition(getState().user, orderId, position)
			.then((data) => dispatch(createUpdatePositionSuccessAction(data)))
			.catch((error) => dispatch(createUpdatePositionErrorAction(error)));
	};
};
