import { supplierClient } from '../client/supplier.client';

export const SELECT_SUPPLIER = 'SELECT_SUPPLIER';

export const FETCH_SUPPLIERS_SUCCESS = 'FETCH_SUPPLIERS_SUCCESS';
export const FETCH_SUPPLIERS_ERROR = 'FETCH_SUPPLIERS_ERROR';

export const createSelectSupplierAction = (supplierId) => {
	return {
		type: SELECT_SUPPLIER,
		supplierId: supplierId
	};
};

export const createFetchSuppliersSuccessAction = (suppliers) => {
	return {
		type: FETCH_SUPPLIERS_SUCCESS,
		suppliers: suppliers
	};
};

export const createFetchSuppliersErrorAction = (error) => {
	console.log(error);
	return {
		type: FETCH_SUPPLIERS_ERROR,
		error: error
	};
};

export const createFetchSuppliersAction = () => {
	return dispatch => {
		supplierClient
			.fetchSuppliers()
			.then((data) => dispatch(createFetchSuppliersSuccessAction(data)))
			.catch((error) => dispatch(createFetchSuppliersErrorAction(error)));
	};
};
