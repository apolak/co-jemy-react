import React from 'react';

export const Currency = React.PropTypes.shape({
	code: React.PropTypes.string,
	unit: React.PropTypes.number
});

export const Dish = React.PropTypes.shape({
	id: React.PropTypes.oneOfType([
		React.PropTypes.number,
		React.PropTypes.string
	]),
	name: React.PropTypes.string,
	price: React.PropTypes.number
});

export const Supplier = React.PropTypes.shape({
	id: React.PropTypes.oneOfType([
		React.PropTypes.number,
		React.PropTypes.string
	]),
	currency: Currency,
	name: React.PropTypes.string,
	menuItems: React.PropTypes.arrayOf(Dish),
	allowCustomItems: React.PropTypes.bool,
	freeDeliveryThreshold: React.PropTypes.number
});

export const Prices = React.PropTypes.shape({
	delivery: React.PropTypes.number,
	packages: React.PropTypes.number,
	positions: React.PropTypes.number,
	total: React.PropTypes.number
});

export const Position = React.PropTypes.shape({
	id: React.PropTypes.string,
	userId: React.PropTypes.string,
	username: React.PropTypes.string,
	dishId: React.PropTypes.oneOfType([
		React.PropTypes.number,
		React.PropTypes.string
	]),
	dishName: React.PropTypes.string,
	price: React.PropTypes.number
});

export const User = React.PropTypes.shape({
	userId: React.PropTypes.string,
	isAdmin: React.PropTypes.bool,
	adminHash: React.PropTypes.string
});

export const Order = React.PropTypes.shape({
	id: React.PropTypes.string,
	supplier: Supplier,
	timeOfOrder: React.PropTypes.number,
	positions: React.PropTypes.arrayOf(Position),
	prices: Prices
});
