import {
	CREATE_ORDER_SUCCESS, FETCH_ORDER_SUCCESS, GRANT_ORDER_ADMIN_SUCCESS
} from '../action/order.action';

export const userIdService = ({ getState }) => (next) => (action) => {
	next(action);

	switch (action.type) {
		case CREATE_ORDER_SUCCESS:
		case FETCH_ORDER_SUCCESS:
			localStorage.setItem('user-id', action.orderResponse.userId);
			break;
		case GRANT_ORDER_ADMIN_SUCCESS:
			localStorage.setItem('user-id', action.userId);
			break;
	}
};
