import { connect } from 'react-redux';
import { SupplierSelectPage } from './supplier-select.page';
import {
	createFetchSuppliersAction,
	createSelectSupplierAction
} from '../../action/supplier.action';
import { push } from 'react-router-redux';

const mapStateToProps = (state) => {
	return {
		suppliers: state.suppliers
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		fetchSuppliers: () => {
			dispatch(createFetchSuppliersAction());
		},
		onSupplierSelect: (supplierId) => {
			dispatch(createSelectSupplierAction(supplierId));
			dispatch(push('/supplier/' + supplierId));
		}
	};
};

export const ConnectedSupplierSelectPage = connect(mapStateToProps, mapDispatchToProps)(SupplierSelectPage);
