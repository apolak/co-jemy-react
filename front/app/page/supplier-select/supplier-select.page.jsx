import * as React from 'react';
import { I18nSupplierSelectorComponent } from '../../component/supplier-selector/supplier-selector.component';
import { ProgressBar } from 'react-toolbox/lib/progress_bar';

import { Supplier } from '../../domain/types';

import styles from './supplier-select.page.scss';

export class SupplierSelectPage extends React.Component {
	componentWillMount() {
		if (!this.hasSuppliers()) {
			this.props.fetchSuppliers();
		}
	}

	hasSuppliers() {
		return this.props.suppliers.length > 0;
	}

	render() {
		if (this.hasSuppliers()) {
			return (
				<div className={styles['supplier-select-page'] + ' ' + styles.page}>
					<div className={styles['supplier-selector']}>
						<I18nSupplierSelectorComponent
							suppliers={this.props.suppliers}
							onSupplierSelect={this.props.onSupplierSelect}
						/>
					</div>
				</div>
			);
		}

		return (
			<div className={styles.page}>
				<div className="loader"><ProgressBar type="linear" mode="indeterminate"/></div>
			</div>
		);
	}
}

SupplierSelectPage.propTypes = {
	suppliers: React.PropTypes.arrayOf(Supplier).isRequired,
	fetchSuppliers: React.PropTypes.func.isRequired,
	onSupplierSelect: React.PropTypes.func.isRequired
};
