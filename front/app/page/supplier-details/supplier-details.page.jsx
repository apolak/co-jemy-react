import React from 'react';
import { SupplierMenuComponent } from '../../component/supplier/supplier-menu.component';
import { ProgressBar } from 'react-toolbox/lib/progress_bar';
import { AppBar } from 'react-toolbox/lib/app_bar';
import { Button } from 'react-toolbox/lib/button';

import { Supplier } from '../../domain/types';

import styles from './supplier-details.page.scss';

export class SupplierDetailsPage extends React.Component {
	componentWillMount() {
		if (!this.props.supplier) {
			this.props.activateSupplier(+(this.props.params.id));
			this.props.fetchSuppliers();
		}
	}

	render() {
		if (this.props.supplier) {
			return (
				<div className={styles.page}>
					<AppBar fixed flat>
						<p className="order-header">Order in {this.props.supplier.name}</p>
						<Button
							className={styles['create-order']}
							accent
							raised
							onClick={this.props.createOrder.bind(this, this.props.supplier.id)}
							label="Create order"
						/>
					</AppBar>
					<SupplierMenuComponent supplier={this.props.supplier}/>
				</div>
			);
		}

		return <div className="page"><ProgressBar type="linear" mode="indeterminate"/></div>;
	}
}

SupplierDetailsPage.propTypes = {
	supplier: Supplier,
	fetchSuppliers: React.PropTypes.func.isRequired,
	createOrder: React.PropTypes.func.isRequired,
	activateSupplier: React.PropTypes.func.isRequired,
	params: React.PropTypes.shape({
		id: React.PropTypes.string.isRequired
	}).isRequired
};
