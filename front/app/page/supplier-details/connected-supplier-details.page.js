import { connect } from 'react-redux';
import { SupplierDetailsPage } from './supplier-details.page';
import { createFetchSuppliersAction, createSelectSupplierAction } from '../../action/supplier.action';
import { createOrderAction } from '../../action/order.action';

const mapStateToProps = (state) => {
	return {
		supplier: state.suppliers.find((supplier) => {
			return supplier.id === state.activeSupplierId;
		})
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		fetchSuppliers: () => {
			dispatch(createFetchSuppliersAction());
		},
		activateSupplier: (supplierId) => {
			dispatch(createSelectSupplierAction(supplierId));
		},
		createOrder: (supplierId) => {
			dispatch(createOrderAction(supplierId));
		}
	};
};

export const ConnectedSupplierDetailsPage = connect(mapStateToProps, mapDispatchToProps)(SupplierDetailsPage);
