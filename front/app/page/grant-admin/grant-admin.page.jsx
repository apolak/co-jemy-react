import React from 'react';
import { AppBar } from 'react-toolbox/lib/app_bar';
import { Button } from 'react-toolbox/lib/button';

import { Order } from '../../domain/types';

import styles from './grant-admin.page.scss';

export class GrantAdminPage extends React.Component {
	componentWillMount() {
		if (!this.props.order) {
			this.props.fetchSuppliers();
			this.props.fetchOrder(this.props.params.id);
		}
	}

	render() {
		return (
			<div className={styles.page}>
				<AppBar fixed flat>
					<p className="grant-admin-header">Pass this url to anyone who should get admin privileges.</p>
				</AppBar>
				<Button
					className={styles['grant-order-admin']}
					accent
					raised
					onClick={this.props.grantAdmin.bind(this, this.props.params.id, this.props.params.hash)}
					label="Get admin"
				/>
			</div>
		);
	}
}

GrantAdminPage.propTypes = {
	order: Order,
	fetchSuppliers: React.PropTypes.func.isRequired,
	fetchOrder: React.PropTypes.func.isRequired,
	params: React.PropTypes.shape({
		id: React.PropTypes.string.isRequired,
		hash: React.PropTypes.string.isRequired
	}).isRequired,
	grantAdmin: React.PropTypes.func.isRequired
};
