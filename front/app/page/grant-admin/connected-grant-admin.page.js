import { connect } from 'react-redux';
import {
	createFetchOrderAction, createGrantOrderAdminAction
} from '../../action/order.action';

import {
	createFetchSuppliersAction
} from '../../action/supplier.action';
import { GrantAdminPage } from './grant-admin.page';

const mapStateToProps = (state) => {
	return {
		order: state.order
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		fetchSuppliers: () => {
			dispatch(createFetchSuppliersAction());
		},
		fetchOrder: (orderId) => {
			dispatch(createFetchOrderAction(orderId));
		},
		grantAdmin: (orderId, hash) => {
			dispatch(createGrantOrderAdminAction(orderId, hash));
		}
	};
};

export const ConnectedGrantAdminPage = connect(mapStateToProps, mapDispatchToProps)(GrantAdminPage);
