import React from 'react';
import { ProgressBar } from 'react-toolbox/lib/progress_bar';
import { PositionDialogComponent } from '../../component/position-dialog/position-dialog.component';
import { Button } from 'react-toolbox/lib/button';
import { AppBar } from 'react-toolbox/lib/app_bar';
import { Link } from 'react-router';

import { Order, User } from '../../domain/types';

import styles from './order-details.page.scss';
import { PositionsListComponent } from '../../component/positions-list/positions-list.component';
import { PriceDetailsComponent } from '../../component/price-details/price-details.component';

export class OrderDetailsPage extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			addDialogActive: false,
			updateDialogActive: false,
			activePosition: null
		};
	}

	componentWillMount() {
		if (!this.props.order) {
			this.props.fetchSuppliers();
			this.props.fetchOrder(this.props.params.id);
		}
	}

	handleUpdate(orderId, position) {
		this.props.updatePosition(orderId, Object.assign({}, this.state.activePosition, position));
	}

	showUpdatePositionDialog(position) {
		this.setState(Object.assign({}, this.state, { activePosition: position, updateDialogActive: true }));
	}

	removePosition(positionId) {
		this.props.removePosition(this.props.order.id, positionId);
	}

	handleToggle(dialogType) {
		this.setState(Object.assign({}, this.state, { [dialogType]: !this.state[dialogType] }));
	}

	render() {
		if (this.props.order) {
			let adminGrantUrl = '';

			if (this.props.user.isAdmin) {
				adminGrantUrl = (
					<Link to={'/orders/' + this.props.order.id + '/grant-admin/' + this.props.user.adminHash}>
						Grant order admin
					</Link>
				);
			}

			return (
				<div className={styles.page}>
					<AppBar fixed flat>
						<p className={styles['order-header']}>
							Order - {this.props.order.supplier.name} {adminGrantUrl}
						</p>
						<Button
							className={styles['add-position'] + ' ' + 'addPosition'}
							icon="add"
							floating
							accent
							mini
							onClick={this.handleToggle.bind(this, 'addDialogActive')}
						/>
					</AppBar>
					<PositionDialogComponent
						onToggle={this.handleToggle.bind(this, 'addDialogActive')}
						onSubmit={this.props.addPosition}
						dishes={this.props.order.supplier.menuItems}
						orderId={this.props.order.id}
						allowCustomItems={this.props.order.supplier.allowCustomItems}
						active={this.state.addDialogActive}
						currency={this.props.order.supplier.currency}
					/>
					<PositionDialogComponent
						onToggle={this.handleToggle.bind(this, 'updateDialogActive')}
						onSubmit={this.handleUpdate.bind(this)}
						dishes={this.props.order.supplier.menuItems}
						orderId={this.props.order.id}
						active={this.state.updateDialogActive}
						position={this.state.activePosition}
						allowCustomItems={this.props.order.supplier.allowCustomItems}
						currency={this.props.order.supplier.currency}
					/>
					<section className={styles.positions}>
						<PositionsListComponent
							user={this.props.user}
							supplier={this.props.order.supplier}
							positions={this.props.order.positions}
							removePosition={this.removePosition.bind(this)}
							showUpdatePositionDialog={this.showUpdatePositionDialog.bind(this)}
						/>
					</section>
					<section className={styles['price-details']}>
						<PriceDetailsComponent
							prices={this.props.order.prices}
							supplier={this.props.order.supplier}
							positions={this.props.order.positions}
						/>
					</section>
				</div>
			);
		}

		return <div className="page"><ProgressBar type="linear" mode="indeterminate"/></div>;
	}
}

OrderDetailsPage.propTypes = {
	order: Order,
	fetchSuppliers: React.PropTypes.func.isRequired,
	fetchOrder: React.PropTypes.func.isRequired,
	updatePosition: React.PropTypes.func.isRequired,
	removePosition: React.PropTypes.func.isRequired,
	addPosition: React.PropTypes.func.isRequired,
	params: React.PropTypes.shape({
		id: React.PropTypes.string.isRequired
	}).isRequired,
	user: User
};
