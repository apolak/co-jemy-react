import { connect } from 'react-redux';
import { OrderDetailsPage } from './order-details.page';
import {
	createFetchOrderAction, createAddPositionAction,
	createRemovePositionAction, createUpdatePositionAction
} from '../../action/order.action';

import {
	createFetchSuppliersAction
} from '../../action/supplier.action';

const mapStateToProps = (state) => {
	return {
		order: state.order,
		user: state.user
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		fetchSuppliers: () => {
			dispatch(createFetchSuppliersAction());
		},
		fetchOrder: (orderId) => {
			dispatch(createFetchOrderAction(orderId));
		},
		addPosition: (orderId, positionData) => {
			dispatch(createAddPositionAction(orderId, positionData));
		},
		removePosition: (orderId, positionId) => {
			dispatch(createRemovePositionAction(orderId, positionId));
		},
		updatePosition: (orderId, position) => {
			dispatch(createUpdatePositionAction(orderId, position));
		}
	};
};

export const ConnectedOrderDetailsPage = connect(mapStateToProps, mapDispatchToProps)(OrderDetailsPage);
