import * as React from 'react';
import { mount, shallow } from "enzyme";
import { expect } from "chai";
import * as sinon from "sinon";

import { PositionsListComponent } from './positions-list.component';
import styles from './positions-list.component.scss';

describe('Position list component', () => {
	it('should display not positions list', () => {
		const props = {
			user: {
				isAdmin: false,
				userId: 'some-id',
				adminHash: ''
			},
			supplier: {
				id: 1,
				currency: {
					code: 'PLN',
					unit: 100
				},
				name: 'Test supplier',
				menuItems: [
					{
						id: 1,
						name: 'test dish',
						price: 1000
					}
				],
				allowCustomItems: false,
				freeDeliveryThreshold: 0
			},
			positions: [
				{
					id: '1',
					userId: '1',
					username: 'Test user',
					dishId: '1',
					price: 1000,
					dishName: 'some dish name'
				}
			],
			prices: {
				delivery: 0,
				packages: 100,
				positions: 1000,
				total: 1100
			},
			removePosition: () => {},
			showUpdatePositionDialog: () => {}
		};

		const wrapper = shallow(<PositionsListComponent {...props} />);
		const body = wrapper.find('p');

		console.log(styles);

		console.log(wrapper.html());
		console.log(wrapper.find('.'+styles.table).length);

		expect(body).to.have.text('Delivery');
	});
});