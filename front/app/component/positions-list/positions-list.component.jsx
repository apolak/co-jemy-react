import React from 'react';
import { PositionRowComponent } from '../position-row/position-row.component';
import { CustomPositionRowComponent } from '../position-row/custom-position-row.component';

import { Position, Supplier, User } from '../../domain/types';

import styles from './positions-list.component.scss';

export class PositionsListComponent extends React.Component {
	render() {
		if (this.props.positions.length === 0) {
			return null;
		}

		return (
			<table className={styles.table}>
				<tbody>
					{
						this.props.positions.map((position, index) => {
							if (position.dishId !== '') {
								return (
									<PositionRowComponent
										key={index}
										className="customDish"
										index={index + 1}
										user={this.props.user}
										currency={this.props.supplier.currency}
										position={position}
										dishes={this.props.supplier.menuItems}
										removePosition={this.props.removePosition.bind(null, position.id)}
										showUpdatePositionDialog={this.props.showUpdatePositionDialog.bind(
											null,
											position
										)}
									/>
								);
							}

							return (
								<CustomPositionRowComponent
									key={index}
									index={index + 1}
									user={this.props.user}
									currency={this.props.supplier.currency}
									position={position}
									removePosition={this.props.removePosition.bind(null, position.id)}
									showUpdatePositionDialog={this.props.showUpdatePositionDialog.bind(null, position)}
								/>
							);
						})
					}
				</tbody>
			</table>

		);
	}
}

PositionsListComponent.propTypes = {
	positions: React.PropTypes.arrayOf(Position).isRequired,
	user: User.isRequired,
	supplier: Supplier.isRequired,
	removePosition: React.PropTypes.func.isRequired,
	showUpdatePositionDialog: React.PropTypes.func.isRequired
};
