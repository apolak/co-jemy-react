import React from 'react';
import { Dialog } from 'react-toolbox/lib/dialog';
import { Dropdown } from 'react-toolbox/lib/dropdown';
import { Input } from 'react-toolbox/lib/input';

import { Currency, Dish, Position } from '../../domain/types';

export class PositionDialogComponent extends React.Component {
	constructor(props) {
		super(props);

		this.actions = [
			{ label: 'Cancel', onClick: this.props.onToggle.bind(this), className: 'cancelButton' },
			{ label: 'Save', onClick: this.handleSubmit.bind(this), className: 'saveButton' }
		];

		this.state = {
			name: '',
			optionId: '',
			optionPrice: '',
			optionName: '',
			displayCustomFields: false
		};
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.position) {
			if (nextProps.position.dishId) {
				this.state = {
					name: nextProps.position.username,
					optionId: nextProps.position.dishId,
					optionName: nextProps.position.dishName,
					optionPrice: nextProps.position.price.toString(),
					displayCustomFields: false
				};
			} else {
				this.state = {
					name: nextProps.position.username,
					optionId: 'custom',
					optionName: nextProps.position.dishName,
					optionPrice: (nextProps.position.price / nextProps.currency.unit).toString(),
					displayCustomFields: true
				};
			}
		}
	}

	handleSubmit() {
		if (this.state.optionId === 'custom') {
			this.props.onSubmit(this.props.orderId, {
				username: this.state.name,
				dishId: '',
				dishName: this.state.optionName,
				price: +(this.state.optionPrice) * this.props.currency.unit
			});
		} else {
			const dish = this.props.dishes.find((dish) => dish.id === +(this.state.optionId));

			this.props.onSubmit(this.props.orderId, {
				username: this.state.name,
				dishId: dish.id,
				dishName: dish.name,
				price: +(dish.price)
			});
		}

		this.props.onToggle();

		this.setState({
			name: '',
			optionId: '',
			optionPrice: '',
			optionName: '',
			displayCustomFields: false
		});
	}

	handleChange(name, value) {
		const changes = {};

		if (name === 'optionId') {
			if (value === 'custom') {
				changes['optionPrice'] = '';
				changes['optionName'] = '';
				changes['displayCustomFields'] = true;
			} else {
				changes['displayCustomFields'] = false;
			}
		}

		changes[name] = value;

		this.setState(Object.assign({}, this.state, changes));
	}

	render() {
		const options = this.props.dishes.map((dish) => {
			return {
				value: dish.id,
				label: dish.name + ' ' + dish.price / this.props.currency.unit + ' ' + this.props.currency.code
			};
		});

		if (this.props.allowCustomItems) {
			options.unshift({
				value: 'custom',
				label: 'Custom order'
			});
		}

		let customInputs = null;

		if (this.state.displayCustomFields) {
			customInputs = (
				<div>
					<Input
						value={this.state.optionName}
						type="text"
						label="Dish name"
						className="dishName"
						maxLength={120}
						onChange={this.handleChange.bind(this, 'optionName')}
					/>
					<Input
						value={this.state.optionPrice}
						type="text"
						label="Dish price"
						className="dishPrice"
						maxLength={30}
						onChange={this.handleChange.bind(this, 'optionPrice')}
					/>
				</div>
			);
		}

		return (
			<div>
				<Dialog
					active={this.props.active}
					onEscKeyDown={this.props.onToggle.bind(this)}
					onOverlayClick={this.props.onToggle.bind(this)}
					actions={this.actions}
				  className="addPositionDialog"
				>
					<section>
						<Input
							value={this.state.name}
							type="text"
							label="Username"
							maxLength={30}
							onChange={this.handleChange.bind(this, 'name')}
						  className="userName"
						/>
						<Dropdown
							value={this.state.optionId}
							auto
							onChange={this.handleChange.bind(this, 'optionId')}
							source={options}
							label="Select dish"
						  className="dishSelect"
						/>
						{customInputs}
					</section>
				</Dialog>
			</div>
		);
	}
}

PositionDialogComponent.propTypes = {
	currency: Currency,
	dishes: React.PropTypes.arrayOf(Dish),
	onToggle: React.PropTypes.func.isRequired,
	onSubmit: React.PropTypes.func.isRequired,
	orderId: React.PropTypes.string.isRequired,
	active: React.PropTypes.bool.isRequired,
	allowCustomItems: React.PropTypes.bool.isRequired,
	position: Position
};
