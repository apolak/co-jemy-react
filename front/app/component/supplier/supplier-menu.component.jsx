import React from 'react';

import styles from './supplier-menu.component.scss';

import { Supplier } from '../../domain/types';

export class SupplierMenuComponent extends React.Component {
	render() {
		return (
			<div className={styles['supplier-menu']}>
				{
					this.props.supplier.menuItems.map((dish) => {
						return (
							<div key={dish.id} className={styles.dish}>
								<p className={styles.name}>{dish.name}</p>
							</div>
						);
					})
				}
			</div>
		);
	}
}

SupplierMenuComponent.propTypes = {
	supplier: Supplier.isRequired
};
