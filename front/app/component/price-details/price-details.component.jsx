import React from 'react';
import { formatPrice } from '../../helper/formatter/price.formatter';
import { DeliveryComponent } from '../delivery/delivery.component';

import { Position, Supplier, Prices } from '../../domain/types';

import styles from './price-details.component.scss';

export class PriceDetailsComponent extends React.Component {
	render() {
		if (this.props.positions.length === 0) {
			return null;
		}

		return (
			<div className={styles.prices}>
				<ul className={styles.list}>
					<li className={styles.item}>
						Positions { formatPrice(this.props.prices.positions, this.props.supplier.currency) }
					</li>
					<li className={styles.item}>
						Packages { formatPrice(this.props.prices.packages, this.props.supplier.currency) }
					</li>
					<li className={styles.item}>
						<DeliveryComponent prices={this.props.prices} supplier={this.props.supplier} />
					</li>
					<li className={styles.item}>
						Total { formatPrice(this.props.prices.total, this.props.supplier.currency) }
					</li>
				</ul>
			</div>
		);
	}
}

PriceDetailsComponent.propTypes = {
	prices: Prices.isRequired,
	supplier: Supplier.isRequired,
	positions: React.PropTypes.arrayOf(Position).isRequired
};
