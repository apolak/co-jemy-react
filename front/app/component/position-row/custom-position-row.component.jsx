import React from 'react';
import { Button } from 'react-toolbox/lib/button';
import { formatPrice } from '../../helper/formatter/price.formatter';

import { User, Position, Currency } from '../../domain/types';

import styles from './position-row.component.scss';

export class CustomPositionRowComponent extends React.Component {
	render() {
		let options = <td className={styles.options} />;

		if (this.props.position.userId === this.props.user.userId || this.props.user.isAdmin) {
			options = (
				<td className={styles.options}>
					<Button
						className={styles.button}
						icon="mode_edit"
						floating accent
						mini
						onClick={this.props.showUpdatePositionDialog}
					/>
					<Button
						className={styles.button}
						icon="delete"
						floating
						accent
						mini
						onClick={this.props.removePosition}
					/>
				</td>
			);
		}

		return (
			<tr className={styles.position + ' ' + 'customDish'}>
				<td className={styles.index}>{ this.props.index }</td>
				<td className={styles.username}>{ this.props.position.username }</td>
				<td className={styles.dishName}>{ this.props.position.dishName}</td>
				<td className={styles.price}>{ formatPrice(this.props.position.price, this.props.currency) }</td>
				{options}
			</tr>
		);
	}
}

CustomPositionRowComponent.propTypes = {
	user: User.isRequired,
	position: Position.isRequired,
	index: React.PropTypes.number.isRequired,
	showUpdatePositionDialog: React.PropTypes.func.isRequired,
	removePosition: React.PropTypes.func.isRequired,
	currency: Currency.isRequired
};
