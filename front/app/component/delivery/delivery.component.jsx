import React from 'react';
import { formatPrice } from '../../helper/formatter/price.formatter';
import { Supplier, Prices } from '../../domain/types';

export class DeliveryComponent extends React.Component {
	constructor(props) {
		super(props);

		if (props.supplier.freeDeliveryThreshold > 0) {
			const freeDeliveryThreshold = props.supplier.freeDeliveryThreshold - props.prices.positions;
			this.state = {
				neededToFreeDelivery: (freeDeliveryThreshold < 0) ? 0 : freeDeliveryThreshold
			};
		} else {
			this.state = {
				neededToFreeDelivery: 0
			};
		}
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.supplier.freeDeliveryThreshold > 0) {
			const freeDeliveryThreshold = nextProps.supplier.freeDeliveryThreshold - nextProps.prices.positions;
			this.setState({
				neededToFreeDelivery: (freeDeliveryThreshold < 0) ? 0 : freeDeliveryThreshold
			});
		}
	}

	render() {
		if (this.state.neededToFreeDelivery === 0) {
			return <p>Free delivery</p>;
		}

		return (
			<p>
				<b>Delivery</b> { formatPrice(this.props.prices.delivery, this.props.supplier.currency) } <br/>
				<b>Buy for {
					formatPrice(this.state.neededToFreeDelivery, this.props.supplier.currency)
				} more and get free delivery</b>
			</p>
		);
	}
}

DeliveryComponent.propTypes = {
	prices: Prices.isRequired,
	supplier: Supplier.isRequired
};
