import React from 'react';
import { Dropdown } from 'react-toolbox/lib/dropdown';
import { injectIntl, intlShape } from 'react-intl';

import { Supplier } from '../../domain/types';

export class SupplierSelectorComponent extends React.Component {
	onChange(value) {
		this.props.onSupplierSelect(+(value));
	}

	render() {
		const supplierItems = this.props.suppliers.map((supplier) => {
			return {
				value: supplier.id,
				label: supplier.name
			};
		});

		const label = this.props.intl.formatMessage({
			id: 'supplier-selector.placeholder',
			defaultMessage: 'Default dziala'
		});

		return <Dropdown className="supplierSelect" auto onChange={this.onChange.bind(this)} source={supplierItems} label={label}/>;
	}
}

export const I18nSupplierSelectorComponent = injectIntl(SupplierSelectorComponent);

SupplierSelectorComponent.propTypes = {
	suppliers: React.PropTypes.arrayOf(Supplier).isRequired,
	onSupplierSelect: React.PropTypes.func.isRequired,
	intl: intlShape.isRequired
};
