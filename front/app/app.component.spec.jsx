import React from 'react';
import { shallow } from "enzyme";
import { expect } from "chai";

import AppComponent from './app.component';
import styles from './app.component.scss';

describe('App component', () => {
	it('should display text', () => {
		const wrapper = shallow(<AppComponent />);
		const body = wrapper.find('.' + styles.app);

		expect(body.find('p')).to.have.text('Test text');
	});
});