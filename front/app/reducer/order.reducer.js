import update from 'immutability-helper';

import {
	CREATE_ORDER_SUCCESS, FETCH_ORDER_SUCCESS, ADD_POSITION_SUCCESS, REMOVE_POSITION_SUCCESS, UPDATE_POSITION_SUCCESS
} from '../action/order.action';

export const orderReducer = (state = [], action) => {
	switch (action.type) {
		case CREATE_ORDER_SUCCESS:
			return {
				id: action.orderResponse.id,
				supplier: action.orderResponse.supplier,
				timeOfOrder: action.orderResponse.timeOfOrder,
				positions: action.orderResponse.positions,
				prices: action.orderResponse.prices
			};
		case FETCH_ORDER_SUCCESS:
			return {
				id: action.orderResponse.id,
				supplier: action.orderResponse.supplier,
				timeOfOrder: action.orderResponse.timeOfOrder,
				positions: action.orderResponse.positions,
				prices: action.orderResponse.prices
			};
		case ADD_POSITION_SUCCESS:
			return update(state, {
				positions: {
					$push: [action.payload.position]
				},
				prices: {
					$set: action.payload.prices
				}
			});
		case REMOVE_POSITION_SUCCESS: {
			const positionIndex = state.positions.findIndex((item) => item.id === action.payload.position.id);

			return update(state, {
				positions: {
					$splice: [[positionIndex, 1]]
				},
				prices: {
					$set: action.payload.prices
				}
			});
		}
		case UPDATE_POSITION_SUCCESS: {
			const positionIndex = state.positions.findIndex((item) => item.id === action.payload.position.id);

			return update(state, {
				positions: {
					[positionIndex]: {
						$set: action.payload.position
					}
				},
				prices: {
					$set: action.payload.prices
				}
			});
		}
		default:
			return state;
	}
};
