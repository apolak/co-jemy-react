import { combineReducers } from 'redux';
import { activeSupplierReducer } from './active-supplier.reducer';
import { suppliersReducer } from './suppliers.reducer';
import { userReducer } from './user.reducer';
import { orderReducer } from './order.reducer';
import { intlReducer } from 'react-intl-redux';
import { routerReducer } from 'react-router-redux';

export const reducers = combineReducers({
	intl: intlReducer,
	activeSupplierId: activeSupplierReducer,
	suppliers: suppliersReducer,
	user: userReducer,
	order: orderReducer,
	routing: routerReducer
});
