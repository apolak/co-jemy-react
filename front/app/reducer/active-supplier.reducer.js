import {
	SELECT_SUPPLIER, FETCH_ORDER_SUCCESS
} from '../action/supplier.action';

export const activeSupplierReducer = (state = [], action) => {
	switch (action.type) {
		case SELECT_SUPPLIER:
			return action.supplierId;
		case FETCH_ORDER_SUCCESS:
			return action.orderResponse.supplier.id;
		default:
			return state;
	}
};
