import {
	FETCH_SUPPLIERS_SUCCESS
} from '../action/supplier.action';

export const suppliersReducer = (state = [], action) => {
	switch (action.type) {
		case FETCH_SUPPLIERS_SUCCESS:
			return action.suppliers;
		default:
			return state;
	}
};
