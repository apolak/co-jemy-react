import {
	CREATE_ORDER_SUCCESS, FETCH_ORDER_SUCCESS, GRANT_ORDER_ADMIN_SUCCESS
} from '../action/order.action';

export const userReducer = (state = [], action) => {
	switch (action.type) {
		case FETCH_ORDER_SUCCESS:
		case CREATE_ORDER_SUCCESS:
			return {
				isAdmin: action.orderResponse.isAdmin,
				userId: action.orderResponse.userId,
				adminHash: (action.orderResponse.isAdmin) ? action.orderResponse.adminHash : null
			};
		case GRANT_ORDER_ADMIN_SUCCESS:
			return {
				isAdmin: action.user.isAdmin,
				userId: action.user.userId,
				adminHash: action.user.adminHash
			};
		default:
			return state;
	}
};
