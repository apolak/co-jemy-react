import React from 'react';
import ReactDom from 'react-dom';

import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';

import { createStore, applyMiddleware } from 'redux';

import { Provider } from 'react-intl-redux';
import { addLocaleData } from 'react-intl';
import plLocaleData from 'react-intl/locale-data/pl';
import enLocaleData from 'react-intl/locale-data/en';
import { Router, browserHistory } from 'react-router';
import { syncHistoryWithStore, routerMiddleware } from 'react-router-redux';
import thunk from 'redux-thunk';

import { userIdService } from './service/user-id.service';

import { getLocale } from './helper/converter/query-to-locale.converter';
import { reducers } from './reducer/index';
import { routes } from './config/routes';
import { translations } from './config/translations';

import 'react-toolbox/lib/commons.scss';

addLocaleData([
	...enLocaleData,
	...plLocaleData
]);

const locale = getLocale(location.search);

const initialState = {
	intl: {
		defaultLocale: 'en',
		locale: locale,
		messages: translations[locale]
	},
	suppliers: [],
	activeSupplierId: null,
	user: null,
	order: null
};

const store = createStore(
	reducers,
	initialState,
	composeWithDevTools(
		applyMiddleware(thunk),
		applyMiddleware(userIdService),
		applyMiddleware(routerMiddleware(browserHistory))
	)
);

const history = syncHistoryWithStore(browserHistory, store);

if (typeof window !== 'undefined') {
	ReactDom.render(
		<Provider store={store}>
			<Router history = {history} routes={routes}/>
		</Provider>,
		document.getElementById('root')
	);
}
