import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { locationShape } from 'react-router/lib/PropTypes';

import styles from './app.component.scss';

export default class AppComponent extends React.Component {
	render() {
		const transitionName = (this.props.location.action === 'POP') ? 'reversePageSwap' : 'pageSwap';
		return (
			<div className={styles['app-container']}>
				<ReactCSSTransitionGroup
					transitionName={transitionName}
					transitionEnterTimeout={200}
					transitionLeaveTimeout={200}
				>
					{React.cloneElement(this.props.children, {
						key: this.props.location.pathname
					})}
				</ReactCSSTransitionGroup>
			</div>
		);
	}
}

AppComponent.propTypes = {
	location: locationShape.isRequired,
	children: React.PropTypes.node
};
