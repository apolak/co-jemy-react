export const formatPrice = (price, currency) => {
	return price / currency.unit + ' ' + currency.code;
};
