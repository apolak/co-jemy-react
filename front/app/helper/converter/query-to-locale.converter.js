export const getLocale = (queryString) => {
	const params = {};

	if (queryString) {
		const parts = queryString.substring(1).split('&');

		for (let i = 0; i < parts.length; i++) {
			const nv = parts[i].split('=');
			if (!nv[0]) continue;
			params[nv[0]] = nv[1] || true;
		}
	}

	return params['lang'] || 'en';
};
