var chai = require('chai');
var chaiEnzyme = require('chai-enzyme');
var chaiJsx = require('chai-jsx');
var jsdom = require('jsdom').jsdom;
var hook = require('css-modules-require-hook');
var sass = require('node-sass');
var path = require('path');

hook({
	extensions: ['.scss'],
	preprocessCss: function (css, filepath) {
		var result =  sass.renderSync({
			data: css,
			includePaths: [ path.resolve(filepath, '..') ]
		});

		return result.css;
	}
});

chai.use(chaiEnzyme());
chai.use(chaiJsx);

global.document = jsdom('<!doctype html><html><body></body></html>');
global.window = document.defaultView;
global.navigator = global.window.navigator;
