module.exports = [
	{
		"id": 1,
		"name": "Sushi Kushi",
		"singlePackageCost": 1000,
		"currency": {
			"code": 'PLN',
			"unit": 100
		},
		"deliveryCost": 400,
		"freeDeliveryThreshold": 30000,
		"phoneNumber": "111222333",
		"websiteUrl": "wp.pl",
		"menuUrl": "wp.pl",
		"allowCustomItems": true,
		"menuItems": [
			{
				"id": 1,
				"name": "Philadelphia",
				"price": 2300
			}
		]
	},
	{
		"id": 2,
		"currency": {
			"code": 'PLN',
			"unit": 100
		},
		"singlePackageCost": 500,
		"name": "Knajpa u heroku",
		"deliveryCost": 1000,
		"freeDeliveryThreshold": 10000,
		"phoneNumber": "123123123",
		"websiteUrl": "http://google.com",
		"menuItems": [],
		"menuUrl": null,
		"allowCustomItems": true,
	},
	{
		"id": 3,
		"currency": {
			"code": 'PLN',
			"unit": 100
		},
		"name": "Trio",
		"deliveryCost": 0,
		"freeDeliveryThreshold": 0,
		"singlePackageCost": 1000,
		"phoneNumber": "666 888 777",
		"websiteUrl": "http://www.triocatering.pl/",
		"menuUrl": "http://www.triocatering.pl/bar_triocatering.php",
		"menuItems": [
			{
				"id": 2,
				"name": "sznycel",
				"price": 1500
			}
		],
		"allowCustomItems": false,
	}
];
