'use strict';

const uuid4 = require('uuid4');

module.exports = (userId) => {
	return (userId) ? uuid4.valid(userId) ? userId : uuid4() : uuid4();
};
