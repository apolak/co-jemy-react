'use strict';

const uuid4 = require('uuid4');
const Status = require('./status');
const Prices = require('./prices');
const Positions = require('./positions');

class Order {
	constructor(userId, supplier) {
		this.admins = [userId];
		this.positions = new Positions();
		this.id = uuid4();
		this.userId = userId;
		this.supplier = supplier;
		this.adminHash = uuid4();
		this.creationDate = Date.now();
		this.status = Status.OPEN;
		this.prices = new Prices(supplier.deliveryCost, supplier.freeDeliveryThreshold, supplier.singlePackageCost);
	}

	addAdmin(userId) {
		if (this.admins.findIndex((item) => item === userId) < 0) {
			this.admins.push(userId);
		}
	}

	addPosition(userId, position) {
		if (this.status === Status.OPEN) {
			return this.positions.addPosition(userId, position);
		} else {
			throw new Error('You can add position only to opened orders');
		}
	}

	updatePosition(positionId, userId, position) {
		const currentPosition = this.positions.getPosition(positionId);

		if (currentPosition.userId === userId || this.isAdmin(userId)) {
			return this.positions.updatePosition(positionId, position);
		}
	}

	removePosition(positionId, userId) {
		const currentPosition = this.positions.getPosition(positionId);

		if (currentPosition.userId === userId || this.isAdmin(userId)) {
			return this.positions.removePosition(positionId);
		}
	}

	close() {
		if (this.status === Status.OPEN) {
			this.status = Status.CLOSED;
		} else {
			throw new Error('Only opened order can be closed');
		}
	}

	pay() {
		if (this.status === Status.PAID) {
			this.status = Status.PAID;
		} else {
			throw new Error('Only closed order can be paid');
		}
	}

	isAdmin(userId) {
		return this.admins.find((item) => item === userId) !== undefined;
	}

	getPrices() {
		return this.prices.calculate(this.positions.getAll());
	}

	toJSON() {
		return {
			adminHash: this.adminHash,
			supplier: this.supplier,
			id: this.id,
			timeOfOrder: this.creationDate,
			status: this.status,
			positions: this.positions.toJSON(),
			prices: this.getPrices()
		}
	}
}

module.exports = Order;
