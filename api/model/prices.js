class Prices {
	constructor(deliveryCost, freeDeliveryThreshold, packageCost) {
		this.freeDeliveryThreshold = freeDeliveryThreshold;
		this.packageCost = packageCost;
		this.deliveryCost = deliveryCost;
	}

	calculate(positions) {
		const positionsTotalPrice = positions.reduce((total, currentPosition) => {
				return total + currentPosition.price
			}, 0);

		const packagesTotalPrice = this.packageCost * positions.length;

		const deliveryCost = this.calculateDeliveryCost(positionsTotalPrice);

		return {
			delivery: deliveryCost,
			packages: packagesTotalPrice,
			positions: positionsTotalPrice,
			total: deliveryCost + positionsTotalPrice + packagesTotalPrice
		}
	}

	calculateDeliveryCost(positionsTotalPrice) {
		return (positionsTotalPrice >= this.freeDeliveryThreshold) ? 0 : this.deliveryCost;
	}
}

module.exports = Prices;