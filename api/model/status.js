const OPEN = 'OPEN';
const CLOSED = 'CLOSED';
const PAID = 'PAID';

module.exports.OPEN = OPEN;
module.exports.CLOSED = CLOSED;
module.exports.PAID = PAID;