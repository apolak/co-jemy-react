const Immutable = require('immutable');
const uuid4 = require('uuid4');

class Positions {
	constructor() {
		this.positions = Immutable.List();
	}

	findPositionIndex(positionId) {
		return this.positions.findIndex((item) => item.id === positionId);
	}

	getAll() {
		return this.positions.toArray();
	}

	getPosition(positionId) {
		const index = this.findPositionIndex(positionId);

		if (index >= 0) {
			return this.positions.get(index);
		}

		throw new Error('Could not find position with given id');
	}

	addPosition(userId, position) {
		const newPosition = {
			id: uuid4(),
			username: position.username,
			dishId: position.dishId,
			userId: userId,
			price: position.price,
			dishName: position.dishName,
		};

		this.positions = this.positions.push(newPosition);

		return newPosition;
	}

	updatePosition(positionId, position) {
		const index = this.findPositionIndex(positionId);

		if (index >= 0) {
			this.positions = this.positions.update(index, (elem) => {
				return {
					id: elem.id,
					username: position.username,
					dishId: position.dishId,
					userId: elem.userId,
					price: position.price,
					dishName: position.dishName,
				}
			});

			return this.positions.get(index);
		}

		throw new Error('Could not find position with given id');
	}

	removePosition(positionId) {
		const currentPosition = this.positions.findIndex((item) => item.id === positionId);

		if (currentPosition >= 0) {
			const removedPosition = this.getPosition(positionId);

			this.positions = this.positions.delete(currentPosition);

			return removedPosition;
		}

		throw new Error('Could not find position with given id');
	}

	toJSON() {
		return this.positions.toJS();
	}
}

module.exports = Positions;
