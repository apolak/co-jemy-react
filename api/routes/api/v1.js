var express = require('express');
var router = express.Router();

router.use('/suppliers', require('./v1/suppliers.js'));
router.use('/supplier', require('./v1/supplier.js'));
router.use('/orders', require('./v1/orders.js'));

module.exports = router;
