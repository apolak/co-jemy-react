'use strict';

const generateUserId = require('../../../../generator/user-id');
const suppliers = require('../../../../mocks/suppliers');
const Order = require('../../../../model/order');

module.exports = (req, res) => {
	const supplierId = +(req.body.supplierId);

	const supplier = suppliers.find((item) => {
		return item.id === supplierId;
	});

	if (supplier) {
		const userId = generateUserId(req.headers['user-id']);
		const order = new Order(
			userId,
			supplier
		);

		req.app.get('db').set(order.id, order);

		res.json(Object.assign({}, order.toJSON(), {userId: userId, isAdmin: order.isAdmin(userId)}));
	} else {
		res.status(404).json({
			'status': 'not-found',
			'message': 'Could not find supplier'
		});
	}
};
