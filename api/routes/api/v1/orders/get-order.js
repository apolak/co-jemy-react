'use strict';

const generateUserId = require('../../../../generator/user-id');

module.exports = (req, res) => {
	const orderId = req.params.orderId;
	const order = req.app.get('db').get(orderId);

	if (order) {
		const userId = generateUserId(req.headers['user-id']);

		res.json(Object.assign({}, order.toJSON(), {userId: userId, isAdmin: order.isAdmin(userId)}));
	} else {
		res.status(404).json({
			'status': 'not-found',
			'message': 'Could not find order'
		});
	}
};
