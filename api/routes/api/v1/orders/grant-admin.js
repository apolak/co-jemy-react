'use strict';

const generateUserId = require('../../../../generator/user-id');

module.exports = (req, res) => {
	const orderId = req.params.orderId;
	const adminHash = req.params.adminHash;

	const order = req.app.get('db').get(orderId);

	if (order) {
		const userId = generateUserId(req.headers['user-id']);

		if (adminHash === order.adminHash) {
			order.addAdmin(userId);

			req.app.get('db').set(order.id, order);

			res.json({
				orderId: order.id,
				userId: userId,
				isAdmin: order.isAdmin(userId),
				adminHash: order.adminHash
			})
		} else {
			res.status(404).json({
				'status': 'not-found',
				'message': 'Wrong admin hash parameter'
			});
		}
	} else {
		res.status(404).json({
			'status': 'not-found',
			'message': 'Could not find order'
		});
	}
};
