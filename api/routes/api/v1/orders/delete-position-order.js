'use strict';

const generateUserId = require('../../../../generator/user-id');

module.exports = (req, res) => {
	const orderId = req.params.orderId;
	const positionId = req.params.positionId;
	const order = req.app.get('db').get(orderId);

	if (order) {
		const userId = generateUserId(req.headers['user-id']);
		const deletedPosition = order.removePosition(positionId, userId);

		req.app.get('db').set(order.id, order);

		res.json(Object.assign({}, {position: deletedPosition}, {
			prices: order.getPrices()
		}));
	} else {
		res.status(404).json({
			'status': 'not-found',
			'message': 'Could not find order'
		});
	}
};
