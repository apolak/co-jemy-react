var express = require('express');
var router = express.Router();

router.post('/', require ('./orders/post-orders.js'));
router.post('/:orderId/grant/:adminHash', require('./orders/grant-admin.js'));
router.get('/:orderId', require('./orders/get-order.js'));
router.post('/:orderId/close', require('./orders/close-order.js'));
router.post('/:orderId/pay', require('./orders/pay-order.js'));
router.post('/:orderId/positions', require('./orders/add-position-order.js'));
router.put('/:orderId/positions/:positionId', require('./orders/update-position-order.js'));
router.delete('/:orderId/positions/:positionId', require('./orders/delete-position-order.js'));

module.exports = router;
