var express = require('express');
var router = express.Router();

router.get('/', require ('./suppliers/get-suppliers.js'));

module.exports = router;
