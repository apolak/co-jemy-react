var express = require('express');
var router = express.Router();

router.get('/:id', require ('./supplier/get-supplier.js'));

module.exports = router;
