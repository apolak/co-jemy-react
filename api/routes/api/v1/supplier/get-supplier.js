'use strict';

const suppliers = require('../../../../mocks/suppliers');

module.exports = (req, res) => {
	const supplier = suppliers.find((item) => {
		return item.id === +(req.params.id);
	});

	if (supplier) {
		res.json(supplier);
	} else {
		res.status(404).json({
			'status': 'not-found',
			'message': 'Could not find supplier'
		});
	}
};
